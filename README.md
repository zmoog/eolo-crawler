# Eolo Crawler

Collect monthly quota usage pulling data from the Eolo API and pushing it to Mongo.


## Docker

Build the app, the image and create a container to run it:

```bash

$ mvn package

$ docker build -t zmoog/eolo-crawler .

$ docker run -d -p 8080:8080 -e SPRING_DATA_MONGODB_URI=mongodb://harbor.gnu.zg/quota --name eolo-crawler zmoog/eolo-crawler

´´´


