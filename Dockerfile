FROM java:8
MAINTAINER maurizio.branca@gmail.com
EXPOSE 8080
CMD java -jar eolo-crawler-0.0.1-SNAPSHOT.jar
ADD target/eolo-crawler-0.0.1-SNAPSHOT.jar eolo-crawler-0.0.1-SNAPSHOT.jar