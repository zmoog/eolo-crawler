package org.zmoog.eolo.crawler;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.zmoog.eolo.crawler.EoloCrawlerApplication;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = EoloCrawlerApplication.class)
public class EoloCrawlerApplicationTests {

	@Test
	public void contextLoads() {
	}

}
