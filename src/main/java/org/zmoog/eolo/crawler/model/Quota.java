package org.zmoog.eolo.crawler.model;


import java.util.Date;

/**
 * Created by zmoog on 17/10/15.
 */
public class Quota {

    String id;

    Date timestamp;

    Response response;

    Data data;

    Voice voice;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Date getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(Date timestamp) {
        this.timestamp = timestamp;
    }

    public Response getResponse() {
        return response;
    }

    public void setResponse(Response response) {
        this.response = response;
    }

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }

    public Voice getVoice() {
        return voice;
    }

    public void setVoice(Voice voice) {
        this.voice = voice;
    }

    @Override
    public String toString() {
        return "Quota{" +
                "id='" + id + '\'' +
                ", timestamp=" + timestamp +
                ", response=" + response +
                ", data=" + data +
                ", voice=" + voice +
                '}';
    }
}
