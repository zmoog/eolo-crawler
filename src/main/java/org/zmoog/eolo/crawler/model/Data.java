package org.zmoog.eolo.crawler.model;

/**
 * Created by zmoog on 17/10/15.
 */
public class Data {

    int used;

    int quota;

    String nextReset;

    public int getUsed() {
        return used;
    }

    public void setUsed(int used) {
        this.used = used;
    }

    public int getQuota() {
        return quota;
    }

    public void setQuota(int quota) {
        this.quota = quota;
    }

    public String getNextReset() {
        return nextReset;
    }

    public void setNextReset(String nextReset) {
        this.nextReset = nextReset;
    }
}
