package org.zmoog.eolo.crawler.model;

/**
 * Created by zmoog on 17/10/15.
 */
public class Response {

    int status;

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }
}
