package org.zmoog.eolo.crawler.model;

/**
 * Created by zmoog on 17/10/15.
 */
public class Voice {

    int credit;

    public int getCredit() {
        return credit;
    }

    public void setCredit(int credit) {
        this.credit = credit;
    }
}
