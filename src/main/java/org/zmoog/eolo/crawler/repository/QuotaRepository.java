package org.zmoog.eolo.crawler.repository;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.zmoog.eolo.crawler.model.Quota;

/**
 * Created by zmoog on 17/10/15.
 */
public interface QuotaRepository extends MongoRepository<Quota, String> {

}
