package org.zmoog.eolo.crawler;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ImportResource;

@SpringBootApplication
@ImportResource("integration-context.xml")
public class EoloCrawlerApplication {

    public static void main(String[] args) {
        SpringApplication.run(EoloCrawlerApplication.class, args);
    }
}
