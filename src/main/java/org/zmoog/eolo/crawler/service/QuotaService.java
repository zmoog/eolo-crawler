package org.zmoog.eolo.crawler.service;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.integration.annotation.Header;
import org.springframework.integration.annotation.MessageEndpoint;
import org.springframework.integration.annotation.ServiceActivator;
import org.springframework.messaging.handler.annotation.Headers;
import org.zmoog.eolo.crawler.model.Quota;
import org.zmoog.eolo.crawler.repository.QuotaRepository;

import java.util.Date;
import java.util.Map;
import java.util.UUID;

/**
 * Created by zmoog on 17/10/15.
 */
@MessageEndpoint
public class QuotaService {

    @Autowired
    QuotaRepository quotaRepository;

    Log logger = LogFactory.getLog(QuotaService.class);

    @ServiceActivator
    public Quota process(Quota quota, @Headers Map<String, Object> headersMap) {

        logger.info(String.format("headers %s", headersMap));
        logger.info(String.format("Processing %s", quota));

        quotaRepository.save(quota);

        return quota;
    }
}
